import Vue from 'vue'
import App from './App.vue'
import '@/assets/css/tailwind.css'
import { routes } from '@/routes' 
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(VueRouter)
Vue.use(VueResource)

Vue.config.productionTip = false

Vue.http.options.root = "https://sku-management-1dff4.firebaseio.com/"

const router = new VueRouter({
  routes,
  mode : 'history'
})
new Vue({
  render: h => h(App),
  router
}).$mount('#app')
