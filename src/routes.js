import Home from '@/components/Home'
import AddProduct from '@/components/AddProduct'
// import Products from '@/components/Products'
import EditProduct from '@/components/EditProducts'

export const routes = [
    {
        path : '/',
        component : Home
    },
    {
        path : '/add',
        component : AddProduct
    },
    {
        path : '/edit',
        component : EditProduct
    }
]